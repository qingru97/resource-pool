// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github')
const darkCodeTheme = require('prism-react-renderer/themes/dracula')

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'My Resource Pool',
  tagline: 'Shared resources for everyone at CSIT',
  url: 'https://your-docusaurus-test-site.com',
  baseUrl: '/resource-pool/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'en',
    locales: ['en'],
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          routeBasePath: '/',
          sidebarPath: require.resolve('./sidebars.js'),
          editUrl:
            'https://gitlab.com/csitsg/internships/resource-pool/tree/master/',
        },
        blog: {
          showReadingTime: true,
          editUrl:
            'https://gitlab.com/csitsg/internships/resource-pool/tree/master/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      navbar: {
        title: 'Home',
        logo: {
          alt: 'CSIT Logo',
          src: 'img/csit-logo.png',
        },
        items: [
          {
            type: 'doc',
            docId: 'tutorial/intro',
            position: 'left',
            label: 'Tutorial',
          },
          { to: '/blog', label: 'Sample Blog', position: 'left' },
          {
            type: 'doc',
            docId: 'general/build-tools/gradle',
            position: 'left',
            label: 'Common',
          },
          {
            type: 'doc',
            docId: 'database/intro',
            position: 'left',
            label: 'Database',
          },
          {
            type: 'doc',
            docId: 'software-engineering/general/good-engineer',
            position: 'left',
            label: 'Software Engineering',
          },
          {
            href: 'https://gitlab.com/csitsg/internships/resource-pool/tree/master/',
            label: 'GitLab',
            position: 'right',
          },
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'Docs',
            items: [
              {
                label: 'Tutorial',
                to: 'tutorial/intro',
              },
              {
                label: 'Database',
                to: 'database/intro',
              },
              {
                label: 'General',
                to: 'general/intro',
              },
              {
                label: 'Software Engineering',
                to: 'software-engineering/intro',
              },
            ],
          },
          {
            title: 'Community',
            items: [
              {
                label: 'Stack Overflow',
                href: 'https://stackoverflow.com/questions/tagged/docusaurus',
              },
              {
                label: 'Discord',
                href: 'https://discordapp.com/invite/docusaurus',
              },
              {
                label: 'Twitter',
                href: 'https://twitter.com/docusaurus',
              },
            ],
          },
          {
            title: 'More',
            items: [
              {
                label: 'Blog',
                to: '/blog',
              },
              {
                label: 'GitLab',
                href: 'https://gitlab.com/csitsg/internships/resource-pool/tree/master/',
              },
            ],
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()} My Resource Pool, Inc. Built with Docusaurus.`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
}

module.exports = config
