// @ts-check

/** @type {import('@docusaurus/plugin-content-docs').SidebarsConfig} */
const sidebars = {
  tutorial: [
    { label: 'Introduction', type: 'doc', id: 'tutorial/intro' },
    {
      label: 'Basics',
      type: 'category',
      items: [
        {
          label: 'Create a Page',
          type: 'doc',
          id: 'tutorial/basics/create-a-page',
        },
        {
          label: 'Create a Document',
          type: 'doc',
          id: 'tutorial/basics/create-a-document',
        },
        {
          label: 'Create a Blog Post',
          type: 'doc',
          id: 'tutorial/basics/create-a-blog-post',
        },
        {
          label: 'Markdown Features',
          type: 'doc',
          id: 'tutorial/basics/markdown-features',
        },
        {
          label: 'Deploy your site',
          type: 'doc',
          id: 'tutorial/basics/deploy-your-site',
        },
        {
          label: 'Congratulations',
          type: 'doc',
          id: 'tutorial/basics/congratulations',
        },
      ],
    },
    {
      label: 'Extras',
      type: 'category',
      items: [
        {
          label: 'Manage Doc Versions',
          type: 'doc',
          id: 'tutorial/extras/manage-docs-versions',
        },
      ],
    },
  ],
  softwareEngineering: [
    {
      label: 'General',
      type: 'category',
      items: [
        'software-engineering/general/good-engineer',
        {
          label: 'Agile',
          type: 'category',
          items: ['software-engineering/general/agile/user-stories'],
        },
      ],
    },
    {
      label: 'Best Practices',
      type: 'category',
      items: ['software-engineering/best-practices/intro'],
    },
    {
      label: 'Architecture',
      type: 'category',
      items: [
        'software-engineering/architecture/architectural-styles',
        'software-engineering/architecture/design-patterns',
      ],
    },
    {
      label: 'Backend',
      type: 'category',
      items: [
        'software-engineering/backend/intro',
        {
          label: 'Java',
          type: 'category',
          items: ['software-engineering/backend/java/intro'],
        },
        {
          label: 'RESTful APIs',
          type: 'category',
          items: ['software-engineering/backend/restful-apis/intro'],
        },
        {
          label: 'Spring',
          type: 'category',
          items: ['software-engineering/backend/spring/intro'],
        },
      ],
    },
    {
      label: 'Frontend',
      type: 'category',
      items: ['software-engineering/frontend/intro'],
    },
  ],
  general: [
    { label: 'Introduction', type: 'doc', id: 'general/intro' },
    {
      label: 'Build Tools',
      type: 'category',
      items: ['general/build-tools/gradle'],
    },
    {
      label: 'Containerisation',
      type: 'category',
      items: [
        {
          label: 'Docker',
          type: 'category',
          items: ['general/containerisation/docker/intro'],
        },
        {
          label: 'Kubernetes',
          type: 'category',
          items: ['general/containerisation/kubernetes/intro'],
        },
      ],
    },
    {
      label: 'Version Control',
      type: 'category',
      items: [
        'general/version-control/git',
        'general/version-control/git-branching',
        'general/version-control/gitlab',
      ],
    },
  ],
  database: [
    {
      label: 'Introduction',
      type: 'doc',
      id: 'database/intro',
    },
    {
      label: 'Document Database',
      type: 'category',
      items: ['database/document-database/mongo'],
    },
    {
      label: 'Graph Database',
      type: 'category',
      items: [
        'database/graph-database/neo4j',
        'database/graph-database/tigergraph',
      ],
    },
    {
      label: 'Relational Database',
      type: 'category',
      items: ['database/relational-database/sql'],
    },
  ],
}

module.exports = sidebars
