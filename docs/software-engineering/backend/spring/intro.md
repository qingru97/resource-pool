# Introduction

Placeholder for Spring-related resources

## Fundamentals

- Bean Lifecycle

## Common Topics

1. Spring Boot
2. Spring Security
3. Spring Data
   - To implement data access layers for various persistence stores
     a. Spring Data JPA
     b. Spring Data JDBC

## Other Core Topics

1. Spring Aspect Oriented Programming (AOP)
   - Cross-cutting concerns

## Resources

### Books

- Spring in Action
