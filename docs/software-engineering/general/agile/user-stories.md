# User Stories

## Resources

- [Themes, Epics, Stories, Tasks](https://www.aha.io/roadmapping/guide/agile/themes-vs-epics-vs-stories-vs-tasks)
- [How to write a good user story with examples](https://stormotion.io/blog/how-to-write-a-good-user-story-with-examples-templates/)
- [Tricks for writing user stories like a pro](https://medium.com/agileinsider/tips-tricks-for-writing-user-stories-like-a-pro-product-owner-guide-part-ii-fc9fa0c6538a)
