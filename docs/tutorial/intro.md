---
sidebar_position: 1
---

# Introduction

Thank you for being interested in contributing to this resource pool that everyone can benefit from.

_**Open to suggestions on how to further improve and organize this site :)**_

## Objectives

The main objectives is to help our newer colleagues/interns to onboard more smoothly while also providing a common place for useful resources to be shared among each other.

The content of this website can include:

- Core topics and best practices in each specialization area
- Common technologies in each specialization area
- Useful links to books, blogs, documentations, tutorials, videos

## Getting Started

1. Installation
   - Ensure that you have [Node.js](https://nodejs.org/en/download/) version 14 or above
2. Get familiarize with the use of Docusarus
   - Read through the official tutorial provided by Docusarus in this sidebar: [Tutorial - Basics](/docs/category/tutorial---basics) and [Tutorial - Extras](/docs/categorytutorial---extras)
