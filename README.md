# Resource Pool

This resource pool platform is built using [Docusaurus 2](https://docusaurus.io/), a modern static website generator.

## Objectives

An initiative to start a common platform for resources to be shared among everyone at CSIT, mainly to provide a better onboarding experience for new officers and interns.

## Setup

### Installation

```
$ yarn
```

### Local Development

```
$ yarn start
```

This command starts a local development server and opens up a browser window. Most changes are reflected live without having to restart the server.

### Build

```
$ yarn build
```

This command generates static content into the `build` directory and can be served using any static contents hosting service.

### Deployment

Using SSH:

```
$ USE_SSH=true yarn deploy
```

Not using SSH:

```
$ GIT_USER=<Your GitHub username> yarn deploy
```

## How to contribute

Please read [Tutorial](/docs/tutorial/intro.md) to get started on contributing to this resource.
